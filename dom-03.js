'use strict';


window. onload =() => {

//  1.1 Añade un botón a tu html con el id btnToClick y en tu javascript añade el 
// evento click que ejecute un console log con la información del evento del click   
    const addListeners = () => {

        const btn = document.getElementById('btnToClick');

        let addClick = () => {
            console.log('evento de boton');
        }

        btn.addEventListener('click', addClick);
        
    }
addListeners();
// 1.2 Añade un evento 'focus' que ejecute un console.log con el valor del input.
const focus = () => {

        const btn2 = document.querySelector('.focus');

        let addClick = () => {
            console.log('evento de focus');
        }

        btn2.addEventListener('focus', addClick);
        
    }
focus();


// 1.3 Añade un evento 'input' que ejecute un console.log con el valor del input.
const input = () => {

        const btn2 = document.querySelector('.value');

        let addClick = () => {
            console.log('evento de input');
        }

        btn2.addEventListener('input', addClick);
        
    }
input();

}